<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectures', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->DateTime("start_time");
            $table->DateTime("end_time");

            $table->String("title");
            $table->String("description");


            $table->UnsignedInteger("video_id")->nullable();
            $table->UnsignedInteger("speaker_id");
            $table->UnsignedInteger("room_id");
            $table->UnsignedInteger("lecture_difficultness_id");
            $table->UnsignedInteger("lecture_lang_id");

            $table->foreign('speaker_id')->references('id')->on('speakers')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('lecture_difficultness_id')->references('id')->on('lecture_difficultnesses')->onDelete('cascade');
            $table->foreign('lecture_lang_id')->references('id')->on('lecture_langs')->onDelete('cascade');
            $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectures');
    }
}
