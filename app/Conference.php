<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Conference extends Model
{
    protected $fillable = ["title"];

    protected $with = ["city", "place"];

    protected $hidden = ["city_id", "place_id", "created_at", "updated_at"];

    public function getPhotoAttribute($value)
    {
        return "http://capstone.svoka.tk/storage/".json_decode($value,true)[0]["download_link"];
    }

    public function place()
    {
        return $this->belongsTo('App\Place');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function lectures() {
        return $this->hasMany('App\Lecture');
    }
}
