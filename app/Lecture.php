<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $hidden = ["created_at", "updated_at", "speaker_id", "room_id", "lecture_lang_id", "lecture_difficultness_id"];

    protected $fillable = ["title", "description", "start_time", "end_time",
        "lecture_difficultness_id",
        "lecture_lang_id",
        "speaker_id",
        "room_id",
        "video_id"

        ];

    protected $with = ["difficulty", "language", "room", "speaker", "platforms"];




    public function difficulty() {
        return $this->belongsTo('App\LectureDifficultness', 'lecture_difficultness_id');
    }

    public function language()
    {
        return $this->belongsTo('App\LectureLang', 'lecture_lang_id');
    }

    public function speaker()
    {
        return $this->belongsTo('App\Speaker');
    }

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    public function video()
    {
        return $this->belongsTo('App\Video');
    }

    public function platforms()
    {
        return $this->belongsToMany('App\LecturePlatform');
    }
}
