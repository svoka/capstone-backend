<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model
{
    protected $hidden = ["created_at", "updated_at"];

    public function getPhotoAttribute($value)
    {
        return "http://capstone.svoka.tk/storage/".json_decode($value,true)[0]["download_link"];
    }
}
