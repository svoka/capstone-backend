<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LecturePlatform extends Model
{
    protected $hidden = ["created_at", "updated_at"];
}
